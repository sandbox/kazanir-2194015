<?php

class views_handler_filter_shared_random extends views_handler_filter_in_operator {

  var $value_form_type = 'text';

  public function construct() {
    parent::construct();
  
  }

  public function init(&$view, &$options) {
    $this->table = $view->base_table;
    $this->field = $view->base_field;
    
    parent::init($view, $options);
    
    $this->table = $view->base_table;
    $this->field = $view->base_field;
    $this->real_field = $view->base_field;
  }
  
  function can_expose() {
    return FALSE;
  }
  
  function can_build_group() {
    return FALSE;
  }
  
  function value_form(&$form, &$form_state) {
    $form['vsr_key'] = array(
      '#type' => 'textfield',
      '#default_value' => (isset($this->options['vsr_key']) ? $this->options['vsr_key'] : $this->view->base_table),
      '#title' => t('Shared Key'),
      '#description' => t('Enter a shared key for other Views which should share or exclude mutual random results.'),
      '#required' => TRUE,
      '#size' => 60,
    );
    
  }
  
  function value_submit($form, &$form_state) {
    $this->options['vsr_key'] = $form_state['values']['options']['vsr_key'];
  }
  
  function validate() {
    $this->get_value_options();
    $errors = array();

    if (empty($this->options['vsr_key'])) {
      $errors[] = t('No shared key provided for filter: @filter', array('@filter' => $this->ui_name(TRUE)));
    }
    return $errors;
  }

  public function query() {
    $st = 'views_shared_random_key_' . (isset($this->options['vsr_key']) ? $this->options['vsr_key'] : $this->base_table);
    $ids = &drupal_static($st,array());
    $this->value = $ids;
  
    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}();
    }

  }









}